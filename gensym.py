""""
Hippety hoppety, V01D-NULLs code is now my property.
https://github.com/V01D-NULL/MoonOS/blob/9e15e2c3fe18d6fa04942aa52a5dc9199448fbb2/scripts/gensym.py
"""

from os import popen, system
import sys

PARSED = "parsed.sym"
KERNEL = sys.argv[1]
writer = open(PARSED, "w+")


def init_writer():
    writer.seek(0)
    writer.truncate()


def write_data(address, function_name):
    if function_name == "":
        return
    data = str(int(str('0x' + address), 0)) + '\0 -> ' + function_name + '\0\n'
    writer.write(data)


# Input: A single line from objdump, returns true if it's a function, otherwise it returns false since we don't need variables or function names
def is_function_symbol(objdump_line):
    if ".text" not in objdump_line:
        return False
    return True


def function_symbol_extract(objdump_line):
    objdump_line = str(objdump_line).split()
    return str(objdump_line[0]), str(objdump_line[5:]).replace("]", "").replace("'","").replace("[", "").replace(",,", ",")


def parse_symbol_tables(unparsed_sym_table):
    unparsed_sym_table = unparsed_sym_table.splitlines()

    writer.write(str(len(unparsed_sym_table) - 1) + '\n')

    for x in range(len(unparsed_sym_table)-1):
        if not is_function_symbol(unparsed_sym_table[x]):
            continue
        else:
            addr, name = function_symbol_extract(unparsed_sym_table[x])
            write_data(str(addr), name)
    write_data(str(0xFFFFFFF0), "Name Not Found")


if __name__ == '__main__':
    init_writer()
    parse_symbol_tables(popen(f"objdump -C -t {KERNEL}").read())
    writer.close()
    print("[*] Wrote symbol table")
